// console.log("saya sedang belajar typescript dasar menggukan function");

function biodata():string {
    return 'my name is wandy';
}
console.log(biodata());

function getUmur(): number {
    return 28;
}
console.log(getUmur());

function getVoid(): void {
    console.log('ini adalah function void');
}
getVoid();

// Function Argument with number
function perkalian(angka1: number, angka2: number): number {
    return angka1 * angka2;
}
const hasil = perkalian(5,5);
console.log(hasil);

// function argument with string
function mahasiswa(nama: string, alamat: string): string{
    return `${nama}, ${alamat}`;
}
const result =  mahasiswa('suwandi amin snagaji','makassar');
console.log(result);


//function as type 
type age = number;
let  age: number= 28;
console.log(age);

// function as type with return
type kurang = (val1: number, val2: number) => number;
let add: kurang = (val1: number, val2: number): number => {
    return val1 - val2;
};
console.log(add(10,5));

// default parameter
let fullName = (fisrtName: string, lastName:string = "amin sangaji"): string => {
    return fisrtName + " " + lastName;
}
console.log(fullName('suwandi'));

// optional Parameter
let dataMahasiswa = (fisrtName: string, lastName?:string): string => {
    return fisrtName + " " + lastName;
}
console.log(dataMahasiswa('suwandi'));

// let tambah = (val1:number, val2?:number): number => {
//     // return val1 + val2;  //ini akan error tapi ketika di compile jalan karena typescript sudah menandai val2 adalah optional
// }
// console.log(tambah(10,10));






