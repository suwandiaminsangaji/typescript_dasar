console.log("saya sedang belajar typescript dasar");

// Language: typescript
// Type: string
let nama: string = "suwandi amin sangaji";
nama = "wandy";

// Type: number
let angka: number = 1;
angka = 1;

// Boolean
let benarSalah: boolean;
benarSalah = true;
benarSalah = false;

// any
let apaAja: any;
apaAja = "suwandi amin snagaji";
apaAja = 1;
apaAja = true;
apaAja = {
    nama: "suwandi amin snagaji",
    umur: 20
};
apaAja = [1, 2, 3];

// Union Type
let noHp : string | number;
noHp = "081212121212";
noHp = 123;


