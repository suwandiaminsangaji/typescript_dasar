console.log("saya sedang belajar typescript dasar menggukan typedata enum");

// enum Bulan {
//     januari,
//     februari,
//     maret,
//     april,
//     mei,
//     juni,
//     juli,
//     agustus,
//     september,
//     oktober,
//     november,
//     desember
// }
// console.log(Bulan);

// enum String
enum Bulan {
    januari = 'januari',
    februari = 'februari',
    maret = 'maret',
    april = 'april',
    mei = 'mei',
    juni = 'juni',
    juli = 'juli',
    agustus = 'agustus',
    september = 'september',
    oktober = 'oktober',
    november = 'november',
    desember = 'desember'
}

console.log(`nama bulan = ${Bulan.april}`);