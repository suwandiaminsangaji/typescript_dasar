console.log("saya sedang belajar typescript dasar menggukan array");

// Array number
let arrayAngka: number[];
arrayAngka = [1, 2, 3];

// Array string
let arrayString: string[];
arrayString = ["suwandi", "amin", "sangaji"];

// array any
let arrayAny: any[];
arrayAny = ["suwandi", "amin", "sangaji", 1, 2, 3];

console.log(arrayAngka);
console.log(arrayString);
console.log(arrayAny);

// tuples
let arrayTuple: [string, number, boolean];
arrayTuple = ["suwandi", 20, true];


// object
let obj: any;
obj = {
    nama: "suwandi amin snagaji",
    umur: 20
}
console.log(`${obj.nama}`);

