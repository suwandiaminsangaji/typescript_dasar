export class User {
    // public nama: string;
    // public umur: number;
    // public alamat:string;
    constructor(public nama: string, public umur: number, public alamat: string) {
        this.nama = nama;
        this.umur = umur;
        this.alamat = alamat;
    }
    setName(value:string):void{
        this.nama = value;
    }
    getName = ():string => {
        return this.nama;
    }
};

// inheritance class / Turunan
class Admin extends User {
    read: boolean = true ;
    write: boolean = true;
    nomorHp: string; //menambahkan constructor baru di turunan class mengunakan super()

    // setter dan getter
    private _email: string = "";

    // static method
    static getRoleName: string = "admin";

    constructor(nama: string, umur: number, alamat: string, nomorHp: string) {
        super(nama, umur, alamat);
        this.nomorHp = nomorHp;
    }

    getrole(): {read: boolean, write: boolean} {
        return {
            read: this.read,
            write: this.write
        };
    };

    // set berguna untuk validasi data
    set email(value: string) {
        if(value.indexOf("@") > -1 || value.length < 5) {
            this._email = value;
        }else{
           this._email = "email tidak valid";
        }
    }
    get email(): string {
        return this._email;
    }
};

let admin = Admin.getRoleName;
console.log(admin)
// let admin = new Admin ('wandy', 28, 'makassar', '081212121212');
// admin.getName();
// admin.setName('suwandi amin sangaji');
// admin.getrole();
// admin.nomorHp;
// console.log(admin);
// admin.email = 'wandy@gmail.com';
// console.log(admin.email);



// let user = new User('suwandi amin snagaji', 28, 'makassar');
// console.log(user);