"use strict";
console.log("saya sedang belajar typescript dasar menggukan typedata enum");
// enum Bulan {
//     januari,
//     februari,
//     maret,
//     april,
//     mei,
//     juni,
//     juli,
//     agustus,
//     september,
//     oktober,
//     november,
//     desember
// }
// console.log(Bulan);
// enum String
var Bulan;
(function (Bulan) {
    Bulan["januari"] = "januari";
    Bulan["februari"] = "februari";
    Bulan["maret"] = "maret";
    Bulan["april"] = "april";
    Bulan["mei"] = "mei";
    Bulan["juni"] = "juni";
    Bulan["juli"] = "juli";
    Bulan["agustus"] = "agustus";
    Bulan["september"] = "september";
    Bulan["oktober"] = "oktober";
    Bulan["november"] = "november";
    Bulan["desember"] = "desember";
})(Bulan || (Bulan = {}));
console.log(`nama bulan = ${Bulan.april}`);
