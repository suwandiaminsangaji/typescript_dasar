"use strict";
console.log("saya sedang belajar typescript dasar menggukan array");
// Array number
let arrayAngka;
arrayAngka = [1, 2, 3];
// Array string
let arrayString;
arrayString = ["suwandi", "amin", "sangaji"];
// array any
let arrayAny;
arrayAny = ["suwandi", "amin", "sangaji", 1, 2, 3];
console.log(arrayAngka);
console.log(arrayString);
console.log(arrayAny);
// tuples
let arrayTuple;
arrayTuple = ["suwandi", 20, true];
// object
let obj;
obj = {
    nama: "suwandi amin snagaji",
    umur: 20
};
console.log(`${obj.nama}`);
