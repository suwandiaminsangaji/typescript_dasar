"use strict";
// console.log("saya sedang belajar typescript dasar menggukan function");
function biodata() {
    return 'my name is wandy';
}
console.log(biodata());
function getUmur() {
    return 28;
}
console.log(getUmur());
function getVoid() {
    console.log('ini adalah function void');
}
getVoid();
// Function Argument with number
function perkalian(angka1, angka2) {
    return angka1 * angka2;
}
const hasil = perkalian(5, 5);
console.log(hasil);
// function argument with string
function mahasiswa(nama, alamat) {
    return `${nama}, ${alamat}`;
}
const result = mahasiswa('suwandi amin snagaji', 'makassar');
console.log(result);
let age = 28;
console.log(age);
let add = (val1, val2) => {
    return val1 - val2;
};
console.log(add(10, 5));
// default parameter
let fullName = (fisrtName, lastName = "amin sangaji") => {
    return fisrtName + " " + lastName;
};
console.log(fullName('suwandi'));
// optional Parameter
let dataMahasiswa = (fisrtName, lastName) => {
    return fisrtName + " " + lastName;
};
console.log(dataMahasiswa('suwandi'));
// let tambah = (val1:number, val2?:number): number => {
//     // return val1 + val2;  //ini akan error tapi ketika di compile jalan karena typescript sudah menandai val2 adalah optional
// }
// console.log(tambah(10,10));
