"use strict";
class Kendaraan {
    start() {
        console.log('mulai');
    }
}
class Mobil extends Kendaraan {
    constructor() {
        super(...arguments);
        this.roda = 4;
    }
}
let mobil = new Mobil();
console.log(mobil.roda);
mobil.start();
class Motor extends Kendaraan {
    constructor() {
        super(...arguments);
        this.roda = 2;
    }
}
let motor = new Motor();
console.log(motor.roda);
motor.start();
