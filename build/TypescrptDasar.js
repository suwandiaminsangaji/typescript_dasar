"use strict";
console.log("saya sedang belajar typescript dasar");
// Language: typescript
// Type: string
let nama = "suwandi amin sangaji";
nama = "wandy";
// Type: number
let angka = 1;
angka = 1;
// Boolean
let benarSalah;
benarSalah = true;
benarSalah = false;
// any
let apaAja;
apaAja = "suwandi amin snagaji";
apaAja = 1;
apaAja = true;
apaAja = {
    nama: "suwandi amin snagaji",
    umur: 20
};
apaAja = [1, 2, 3];
// Union Type
let noHp;
noHp = "081212121212";
noHp = 123;
