"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
class User {
    // public nama: string;
    // public umur: number;
    // public alamat:string;
    constructor(nama, umur, alamat) {
        this.nama = nama;
        this.umur = umur;
        this.alamat = alamat;
        this.getName = () => {
            return this.nama;
        };
        this.nama = nama;
        this.umur = umur;
        this.alamat = alamat;
    }
    setName(value) {
        this.nama = value;
    }
}
exports.User = User;
;
// inheritance class / Turunan
class Admin extends User {
    constructor(nama, umur, alamat, nomorHp) {
        super(nama, umur, alamat);
        this.read = true;
        this.write = true;
        // setter dan getter
        this._email = "";
        this.nomorHp = nomorHp;
    }
    getrole() {
        return {
            read: this.read,
            write: this.write
        };
    }
    ;
    // set berguna untuk validasi data
    set email(value) {
        if (value.indexOf("@") > -1 || value.length < 5) {
            this._email = value;
        }
        else {
            this._email = "email tidak valid";
        }
    }
    get email() {
        return this._email;
    }
}
// static method
Admin.getRoleName = "admin";
;
let admin = Admin.getRoleName;
console.log(admin);
// let admin = new Admin ('wandy', 28, 'makassar', '081212121212');
// admin.getName();
// admin.setName('suwandi amin sangaji');
// admin.getrole();
// admin.nomorHp;
// console.log(admin);
// admin.email = 'wandy@gmail.com';
// console.log(admin.email);
// let user = new User('suwandi amin snagaji', 28, 'makassar');
// console.log(user);
